package com.spring.assign;

import java.util.List;



import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.assign.db.EmployeeModel;
import com.spring.assign.db.model.Employee;

@RestController

public class EmployeeController {

	EmployeeModel model=new EmployeeModel();
	
	@RequestMapping("/saveEmployee")
	public String saveEmployee(@ModelAttribute("emp") Employee emp)
	{
		int result=model.addEmployee(emp);
		if(result>0)return "saved employee "+emp.getId();
		return "error adding employee";
	}
	@RequestMapping("/get/{id}")
	public Employee getEmployee(@PathVariable("id") String id)
	{
		System.out.println(id);
		return model.getEmployee(id);
	}
	
	@RequestMapping("/getAll")
	public List<Employee> getAllEmployees()
	{
		return model.getEmployees();
	}
	
}

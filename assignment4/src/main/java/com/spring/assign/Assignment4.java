package com.spring.assign;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;


@SpringBootApplication
public class Assignment4 {

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Assignment4.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(Assignment4.class, args);
	}
}

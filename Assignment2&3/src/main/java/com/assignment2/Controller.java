package com.assignment2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class Controller {

	
	@RequestMapping("/name")
	public String display() {
		
		return "HDFC BANK";
	}
	@RequestMapping("/branches")
	public ModelAndView display1() {
		
		ModelAndView mav=new ModelAndView("index1");
		
		return mav;
	}
	@RequestMapping("/services")
	public ModelAndView display2() {
		
		ModelAndView mav1=new ModelAndView("index2");
		
		return mav1;
	}
	
	
}

package mainpack;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	
	@RequestMapping("/name")
	public String display() {
		
		return "HDFC BANK";
	}
	
	@RequestMapping("/address")
	public String display1() {
		
		return "Eclipse towers," 
		
				+ "Sarjapura Main"
				+ " road,Bangalore,"
				+ "Karnataka";
	}
	
	
	
}
